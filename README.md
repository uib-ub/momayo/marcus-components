# marcus-components

This repository conatins all queries, templates and extra views for the Marcus webside (marcus.uib.no). The site is based on Semantic-UI. There is a submodule for the search

## Install

```bash
npm install
cd static/semantic/
gulp build
```

## Install Blackbox-search
https://gitlab.com/ubbdev/marcus-search-client

```bash
## static/js/blackbox-search er i .gitignore
cd static/js/
git clone https://gitlab.com/ubbdev/marcus-search-client.git blackbox-search
# Følg installeringsguide til marcus-search-client (https://gitlab.com/ubbdev/marcus-search-client)
# Første steg:
cp default.semantic.json semantic.json
```

