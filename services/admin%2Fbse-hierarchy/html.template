<!DOCTYPE html>
<html lang="no" prefix="og: http://ogp.me/ns#">
<head>
  {%include "../../includes/head_common.inc"%}
  {%include "../../includes/head_page.inc"%}
</head>
<body>
  <button onclick="topFunction()" id="myBtn" title="Tilbake til toppen"><i class="big caret up icon"></i></button>
  {%include "../../includes/sidebar.inc"%}
  {% if lodspk.isAdmin %}    
  {%include "../../includes/static_sidebar_left.inc"%}
  {% endif %}

  <div class="pusher">
    {%include "../../includes/header.inc"%}
    {%include "../../includes/header_mobile.inc"%}
    
    <div class="main-content-wrapper">
      <div class="fullwidth">
        <div class="ui grid">  
          <div class="ui sixteen wide column">
            <h1>BS Register</h1>
            <div id="skos"></div>
            <div id="tree-container"></div>
            <style>
              .node { margin-bottom: 10px; }
              .node rect {
                cursor: pointer;
                fill: #f4f3f1;
                fill-opacity: .5;
                stroke: #f4f3f1;
                stroke-width: 5px;
              }

              .node text {
                font: 14px sans-serif;
                pointer-events: none;
              }

              path.link {
                fill: none;
                stroke: #8aaa67;
                stroke-width: 1.5px;
              }
            </style>

            <script src="http://d3js.org/d3.v3.min.js"></script>
            <script>
              var skosdata = [ 
              { "name" : "Billedsamlingens_emneord", "parent" : "Marcus_emneord" },
              {% for row in models.main %}
              {%if row.name.value == "Billedsamlingens_emneord" %}{%else%}{ "name" : "{{row.name.value}}", "parent" : "{{row.parent.value}}" },{%endif%}{% endfor %}
              ];

              // create a name: node map
              var dataMap = skosdata.reduce(function(map, node) {
                map[node.name] = node;
                return map;
              }, {});

              // create the tree array
              var skostree = [] ;
              skosdata.forEach(function(node) {
                // add to parent
                var parent = dataMap[node.parent];
                if (parent) {
                  // create child array if it doesn't exist
                  (parent.children || (parent.children = []))
                  // add node to child array
                  .push(node);
                } else {
                  // parent is null or missing
                  skostree.push(node);
                }
              });

              // show what we've got
              //d3.select('#tree-container').append('pre')
              //    .text(JSON.stringify(skostree, null, '  '));

              var margin = {top: 30, right: 20, bottom: 30, left: 0},
              width = 800 - margin.left - margin.right,
              barHeight = 25,
              barWidth = width * .35;

              var i = 0,
              duration = 200,
              root;

              var tree = d3.layout.tree()
              .nodeSize([0, 80]);

              var diagonal = d3.svg.diagonal()
              .projection(function(d) { return [d.y, d.x]; });

              var svg = d3.select("#skos").append("svg")
              .attr("width", width + margin.left + margin.right)
              .append("g")
              .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

              root = skostree[0];
              root.x0 = 0;
              root.y0 = 0;
              update(root);

              function update(source) {

                  // Compute the flattened node list. TODO use d3.layout.hierarchy.
                  var nodes = tree.nodes(root);

                  var height = Math.max(500, nodes.length * barHeight + margin.top + margin.bottom);

                  d3.select("svg").transition()
                  .duration(duration)
                  .attr("height", height);

                  d3.select(self.frameElement).transition()
                  .duration(duration)
                  .style("height", height + "px");

                  // Compute the "layout".
                  nodes.forEach(function(n, i) {
                    n.x = i * barHeight;
                  });

                  // Update the nodes…
                  var node = svg.selectAll("g.node")
                  .data(nodes, function(d) { return d.id || (d.id = ++i); });

                  var nodeEnter = node.enter().append("g")
                  .attr("class", "node")
                  .attr("transform", function(d) { return "translate(" + source.y0 + "," + source.x0 + ")"; })
                  .style("opacity", 1e-6);

                  // Enter any new nodes at the parent's previous position.
                  nodeEnter.append("rect")
                  .attr("y", -barHeight / 2)
                  .attr("height", barHeight)
                  .attr("width", barWidth)
                  .style("fill", color)
                  .on("click", click);

                  nodeEnter.append("text")
                  .attr("dy", 3.5)
                  .attr("dx", 5.5)
                  .text(function(d) { return d.name; });

                  // Transition nodes to their new position.
                  nodeEnter.transition()
                  .duration(duration)
                  .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
                  .style("opacity", 1);

                  node.transition()
                  .duration(duration)
                  .attr("transform", function(d) { return "translate(" + d.y + "," + d.x + ")"; })
                  .style("opacity", 1)
                  .select("rect")
                  .style("fill", color);

                  // Transition exiting nodes to the parent's new position.
                  node.exit().transition()
                  .duration(duration)
                  .attr("transform", function(d) { return "translate(" + source.y + "," + source.x + ")"; })
                  .style("opacity", 1e-6)
                  .remove();

                  // Update the links…
                  var link = svg.selectAll("path.link")
                  .data(tree.links(nodes), function(d) { return d.target.id; });

                  // Enter any new links at the parent's previous position.
                  link.enter().insert("path", "g")
                  .attr("class", "link")
                  .attr("d", function(d) {
                    var o = {x: source.x0, y: source.y0};
                    return diagonal({source: o, target: o});
                  })
                  .transition()
                  .duration(duration)
                  .attr("d", diagonal);

                  // Transition links to their new position.
                  link.transition()
                  .duration(duration)
                  .attr("d", diagonal);

                  // Transition exiting nodes to the parent's new position.
                  link.exit().transition()
                  .duration(duration)
                  .attr("d", function(d) {
                    var o = {x: source.x, y: source.y};
                    return diagonal({source: o, target: o});
                  })
                  .remove();

                  // Stash the old positions for transition.
                  nodes.forEach(function(d) {
                    d.x0 = d.x;
                    d.y0 = d.y;
                  });
              }

              // Toggle children on click.
              function click(d) {
                  if (d.children) {
                    d._children = d.children;
                    d.children = null;
                  } else {
                    d.children = d._children;
                    d._children = null;
                  }
                  update(d);
              }

              function color(d) {
                return d._children ? "#3182bd" : d.children ? "#c6dbef" : "#fd8d3c";
              }
            </script>       
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="{{lodspk.home}}js/backtotop/backtotop.js"></script>
  {%include "../../includes/footer.inc"%}
</body>
</html>
