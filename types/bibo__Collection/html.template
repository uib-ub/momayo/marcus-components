<!DOCTYPE html>
<html ng-app="marcus" lang="no" prefix="og: http://ogp.me/ns#">
<head>
  {%include "../../includes/head_common.inc"%}
  {%include "../../includes/head_page.inc"%}
</head>
<body class="col" ng-controller="searchController">
  <button onclick="topFunction()" id="myBtn" title="Tilbake til toppen"><i class="big caret up icon"></i></button>
  {%include "../../includes/sidebar.inc"%}
  {% if lodspk.isAdmin %}    
    {%include "../../includes/static_sidebar_left.inc"%}
  {% endif %}
  
  <div class="pusher">
    {%include "../../includes/header.inc"%}
    {%include "../../includes/header_mobile.inc"%}

    <div class="main-content-wrapper">
      <div class="fullwidth">

        <div class="ui stackable grid">
          <div class="four wide column side">
            <div class="content-info" style="clear:both; display: block; min-height: 150px; margin-bottom: 20px;">
              {% if first.main.logo != null %}
                <img class="ui small left floated image" src="{{first.main.logo.value}}" />
              {% endif %}

              <div class="clear" style="clear:both; "></div>
              <h2>{% for row in models.main %}{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}{% endfor %}</h2>
            </div>
            
            {% if first.description.descriptions.value|length != 0 || models.hierarchy|length != 0 || models.ispartof|length != 0  %}
              <div class="ui top attached tabular menu obj">
                <a class="active item" data-tab="first">Info</a>
                <a class="item" data-tab="second">Filtrér</a>
              </div>
              <div class="ui bottom attached tab white segment active" data-tab="first" style="padding-left: 0px;">            
                {% autoescape off %}{{ first.description.descriptions.value|decodedescription }}{% endautoescape %}

                {% if models.hierarchy|length != 0 %}
                <div class="models-hierarchy">
                  Er en del av:
                  <ul class="ui list">
                  {% for row in models.hierarchy %}
                    <li><a href="{{ row.uri.value }}">{{ row.label.value }}</a></li>
                  {% endfor %}
                  </ul>
                </div>
                {%endif%}

                {% if models.ispartof|length != 0 %}
                <div class="models-ispartof">
                  Knyttet til ...
                  <ul class="ui list">
                  {% for row in models.ispartof %}
                    <li><a href="{{ row.uri.value }}">{{ row.label.value }}</a> <span class="ui label">{{ row.class.value}}</span></li>
                  {% endfor %}
                  </ul>
                </div>
                {%endif%}

              </div>

              <div class="ui bottom attached tab white segment" data-tab="second" style="padding-left: 0px;">
                <div style="margin-top: 20px;">
                  <search-instance-box></search-instance-box>
                  <br>
                  <div ng-cloak id="facet-sidebar" class="ng-cloak four wide column mobile or lower uihidden">
                    <div class="">
                      <facets></facets>
                    </div>
                  </div>
                </div>
              </div>
            {% else %}
             <div>
                <search-instance-box></search-instance-box>
                <br>
                <div ng-cloak id="facet-sidebar" class="ng-cloak four wide column mobile or lower uihidden">
                  <div class="">
                      <facets></facets>
                  </div>
                </div>
              </div>
            {% endif %}
          </div>

          <div class="twelve wide column"> 
            <div class="ui basic segment" style="padding: 0px;">
                <script src="{{lodspk.home}}assets/search/vendor.min.js"></script>
                <script src="{{lodspk.home}}assets/search/app.min.js"></script>
                <br />
                <instance-search-controller-wrapper></instance-search-controller-wrapper>

                <!-- problems minifying the following -->
                <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-translate-interpolation-messageformat/2.11.0/angular-translate-interpolation-messageformat.min.js" type="text/javascript"></script>
                <script src="http://monospaced.github.io/bower-qrcode-generator/js/qrcode.js"></script>


                <!--Constants for this particular page-->
                <script>
                  angular.module("settings", [])
                  .constant("pageSetting", {
                    index: ["{{lodspk.SEARCH_INDEX}}"],
                    appRoot: ["/assets/search/"],
                    blackboxUrl: "{{lodspk.searchAPI}}",
                    persistentFilterOptions: [
                      {
                        "urlParam": null,
                        "filter": "isPartOfId",
                        "value": "{{first.main.uuid.value}}",
                        "description": "Samling",
                        "excludeFacets": [],
                      }
                    ],
                    facets: [
                    {
                      "label": "type",
                      "field": "type.exact",
                      "size": 30,
                      "operator": "OR",
                      "order": "count_desc",
                      "min_doc_count": 0
                    },
                    {
                      "label": "hasGenreForm",
                      "field": "hasGenreForm.exact",
                      "size": 30,
                      "operator": "OR",
                      "order": "count_desc",
                      "min_doc_count": 0
                    },
                    {
                      "field": "hasZoom",
                      "size": 10,
                      "operator": "AND",
                      "order": "term_asc",
                      "min_doc_count": 0
                    },
                    {
                      "field": "isDigitized",
                      "size": 10,
                      "operator": "AND",
                      "order": "count_desc",
                      "min_doc_count": 0
                    },
                    {
                      "field": "subject.exact",
                      "size": 15,
                      "operator": "AND",
                      "min_doc_count": 1
                    },
                    /*{
                      "field": "isPartOf.exact",
                      "size": 15,
                      "operator": "OR",
                      "order": "count_desc",
                      "min_doc_count": 1
                    },*/
                    {
                      "field": "producedIn.exact",
                      "size": 10,
                      "operator": "AND",
                      "order": "count_desc",
                      "min_doc_count": 1
                    },
                    {
                      "field": "maker.exact",
                      "size": 10,
                      "operator": "AND",
                      "order": "count_desc",
                      "min_doc_count": 1
                    }
                        //OBS: Supported intervals: weeks(w), days(d), hours(h), minutes(m), seconds(s)
                        /*{
                         "field": "created",
                         "type" : "date_histogram",
                         "format" : "yyyy",
                         "interval" : "520w",
                         "min_doc_count" : 1,
                         "order" : "key_asc"
                       }*/
                       ],
                       sortOptions: [
                       { value : "", description : "filters.relevance" },
                       { value : "labelSort.exact:asc", description : "filters.labelAsc" },
                       { value : "labelSort.exact:desc", description : "filters.labelDesc" },
                       { value : "identifier:asc", description : "filters.sortAsc" },
                       { value : "identifier:desc", description : "filters.sortDesc" },
                       { value : "available:asc", description : "filters.availableAsc" },
                       { value : "available:desc", description : "filters.availableDesc" },
                       { value : "dateSort:asc", description : "filters.dateAsc" },
                       { value : "dateSort:desc", description : "filters.dateDesc" }
                       ],
                       tags: {
                        "maker" : { queryField: "maker.exact", responseField: "maker", icon: "user", css: "maker"},
                        "collection" : { queryField: "isPartOf.exact", responseField: "isPartOf", icon: "archive", css: "collection"},
                        "event" : { queryField: "producedIn.exact", responseField: "producedIn", icon: "archive", css: "collection"},
                        "spatial" : { queryField: "spatial.exact", responseField: "spatial", icon: "marker", css: "spatial"},
                        "subject" : { queryField: "subject.exact", responseField: "subject", icon: "tag", css: "subject"},
                        "type" : { queryField: "type.exact", responseField: "type", icon: "file outline", css: "type"},
                        "isDigitized" : { queryField: "isDigitized", responseField: "", icon: "", css: "status"},
                        "hasZoom" : { queryField: "hasZoom", responseField: "", icon: "", css: "status"},
                      }
                    });
                    $('.dropdown')
                      .dropdown()
                    ;
                  </script>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript" src="{{lodspk.home}}js/backtotop/backtotop.js"></script>
  {%include "../../includes/footer.inc"%}
</body>
</html>