<div class="simpleCart_shelfItem well" style="border-radius: 0; background-color: #fff;">
  <div class="ui grid">
    <div class="eleven wide column">
      <h4>BESTILL</h4>
      <p>Erverv publiseringsrettigheter eller kjøp trykk av<em><strong>
        {% for row in models.main %}
                  {% if row.title && row.title.value != "" %}
                    {{row.title.value}}
                  {% else %}
                    {% if row.label && row.label.value != "" %}
                      {{row.label.value}}
                    {% else %} 
                      {{row.identifier.value}}
                    {%endif%}
                  {%endif%}
                  {% if row.alternative %}
                    {{row.alternative.value}}
                  {% endif %}
              {% endfor %}
        </strong></em>:
      </p>
      <form id="validate-cart" role="form">
          <input class="item_name" hidden value="{{ first.main.title.value }}">
          <input class="item_link" hidden value="{{ uri }}">
          <input class="item_thumb" hidden value="{{ first.main.thumb.value }}">
          <input class="item_sign" hidden value="{{ first.main.identifier.value }}">
        
        Antall:
        <input type="number" value="1" min="1" max="50" style="width: 40px;" class="form-control item_Quantity" required="true"> 
  
          <select class="item_type">
                <option value="error">Velg alternativ</option>
            <option value="13x18">Trykk 13x18 - 100kr</option>
            <option value="18x24">Trykk 18x24 - 150kr</option>
            <option value="24x30">Trykk 24x30 - 200kr</option>
            <option value="30x40">Trykk 30x40 - 250kr</option>
            <option value="40x50">Trykk 40x50 - 350kr</option>
            <option value="50x60">Trykk 50x60 - 450kr</option>
            <option value="60x70">Trykk 60x70 - 550kr</option>
            <option value="publikasjon">Bruk i publisering - 500kr</option>
          </select>
          <br />
        </p>
        <br />
        <div><span style="margin-top: 8px;" class="item_price"></span></div>

        <div class="ui divider"></div>

        <button class="ui primary button item_add" type="button" style="border-radius: 0;" href="javascript:void(0);"> 
          Legg i handlekurv
        </button>
        <a class="item" href="{{lodspk.home}}cart">
          <i class="shop icon"></i>
          <span class="simpleCart_quantity ui large red label"></span>
        </a>
      </form>
    </div>
    <div class="five wide column">
      {% for row in models.main %}
              <h4>
                {% if row.title && row.title.value != "" %}
                {{row.title.value}}
                {% else %}
                {% if row.label && row.label.value != "" %}
                {{row.label.value}}
                {% else %} 
                {{row.identifier.value}}
                {%endif%}
                {%endif%}
              </h4>
              {% if row.alternative %}
                <h4 itemprop="alternativeHeadline"><small>{{row.alternative.value}}</small></h4>
              {% endif %}
            {% endfor %}

      {% for row in models.digitalresources %}    
              <img class="ui fluid image" src="{{row.imgSM.value}}" data-full="{{row.imgMD.value}}" alt=""/>
          {% endfor %}
    </div>
  </div>
</div>