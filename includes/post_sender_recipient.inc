<div class="ui horizontal list">
  {% if models.sender|length != 0 %}
  {% for row in models.sender %}
  <div class="item"><a href="{{row.uri.value}}">{{ row.label.value }}</a></div>
  {% endfor %}
  {% else %}
  <div class="item">Ukjent</div>
  {% endif %}

  <div class="item"><i class="fa fa-arrow-right"></i> <i class="fa fa-envelope"></i></div>
  
  {% if models.recipient|length != 0 %}
  {% for row in models.recipient %}
  <div class="item"><a href="{{row.uri.value}}">{{ row.label.value }}</a></div>
  {% endfor %}
  {% else %}
  <div class="item">Ukjent</div>
  {% endif %}
</div>