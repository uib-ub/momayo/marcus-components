<div class="ui four doubling stackable cards" style="margin: 20px 0 20px 0 !important;">
  {% for row in models.collections %}
  {%ifchanged row.classHierarchyURI.value%}
  <div class="ui card">
    {% if row.logo %}
    <a class="ui image" href="{{row.uri.value}}">
      <img src="{{row.logo.value}}"/>
    </a>
    {% else %}
    <a href="{{row.classHierarchyURI.value}}" class="ui image">
      <img data-src="holder.js/200x100">
    </a>
    {% endif %}
    <div class="middle aligned content">
      <a class="header" href="{{row.uri.value}}">{% if row.title && row.title.value != "" %}{{row.title.value}}{% else %}{% if row.label && row.label.value != "" %}{{row.label.value}}{% else %} {{row.identifier.value}}{%endif%}{%endif%}</a>

      {% if row.topcollection.value %}
      <p>En del av: <a href="{{row.topcollection.value}}">{{row.toptitle.value}}</a></p>
      {% endif %}
    </div>
  </div>
  {%endifchanged%}
  {% endfor %}
</div>