<div class="ui raised vertical sidebar accordion menu overlay push responsive">

  <div class="item logo">
    <a href="{{lodspk.home}}home">{{lodspk.title}}</a>
  </div>

  <div class="item">
    <form class="ui form" role="search" method="get" action="{{lodspk.home}}search">
      <input type="text" placeholder="Søk ..." name="q">
    </form>
  </div>
  
  <a class="item" href="{{lodspk.home}}collections"><i class="grid layout icon"></i>Samlinger</a>
  <a class="item" href="{{lodspk.home}}search?filter=type.exact%23Bilde"><i class="image icon"></i>Bildesøk</a>
  <a class="item" href="{{lodspk.home}}events"><i class="calendar icon"></i>Hendelser</a>
  <a class="item" href="{{lodspk.home}}exhibitions"><i class="university icon"></i>Utstillinger</a>
  <div class="ui divider"></div>
  <a class="item" href="{{lodspk.home}}genreform"><i class="book icon"></i>Sjanger/form</a>
  <a class="item" href="{{lodspk.home}}conceptschemes"><i class="tags icon"></i>Emneregister</a>
  <a class="item" href="{{lodspk.home}}id/49221d90-3eb7-4d2e-a215-3d4585558dea"><i class="map pin icon"></i>Sted</a>
  <div class="ui divider"></div>
  <a class="item" href="{{lodspk.home}}news"><i class="newspaper outline icon"></i>Nytt</a>
  <a class="item" href="{{lodspk.home}}articles"><i class="print icon"></i>Artikler og fagstoff</a>
  <div class="ui divider"></div>
  <a class="item" href="{{lodspk.home}}search-help"><i class="search icon"></i>Søketips</a>
  <a class="item" href="{{lodspk.home}}resources"><i class="heart icon"></i>Ressurser</a>
  <a class="item disabled" href="{{lodspk.home}}techniques"><i class="newspaper icon"></i>Teknikker</a>
  <a class="item disabled" href="{{lodspk.home}}conceptschemes"><i class="newspaper icon"></i>Andre greier</a>
  <div class="ui divider"></div>
  <a class="item" href="{{lodspk.home}}about-marcus">Om Marcus</a>
  <a class="item" href="{{lodspk.home}}about-collections">Om Spesialsamlingene</a>
  <a class="item" href="{{lodspk.home}}terms-of-use">Bruksvilkår</a>
  <a class="item" href="{{lodspk.home}}stats">Statistikk</a>

</div>