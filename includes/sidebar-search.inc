<div class="ui raised vertical sidebar accordion menu overlay push responsive search" ng-controller="searchController">
    <div>
        <div class="item logo" style="padding:0 !important;">
            <a href="{{lodspk.home}}home">{{lodspk.title}}</a>
        </div>
        <h3 class="ui header">
            <i class="fa fa-calendar"></i>
            <div class="content" translate="filters.created"></div>
        </h3>
        <date-range> </date-range>
        <!--Facet status as a dropdown -->
        <h3 class="ui header">
            <i class="fa fa-certificate"></i>
            <div class="content" translate="searchContainer.statusFilter"></div>
        </h3>
        <facet type="hasZoom" data="results"></facet>
        <facet type="isDigitized" data="results"></facet>

        <!-- Types facets without dropdown checkboxes-->
        <h3 class="ui header">
            <i class="fa fa-file-o"></i>
            <div class="content" translate="filters.type"></div>
        </h3>
        <facet type="type.exact" data="results"></facet>

        <!-- Topic facets-->
        <h3 class="ui header">
            <i class="fa fa-tag"></i>
            <div class="content" translate="filters.concept"></div>
        </h3>
        <facet type="subject.exact" data="results"></facet>

        <!-- Collection facets-->
        <h3 class="ui header">
            <i class="fa fa-archive"></i>
            <div class="content" translate="filters.partOf"></div>
        </h3>
        <facet type="isPartOf.exact" data="results"></facet>

        <!-- Events facets-->
        <h3 class="ui header">
            <i class="fa fa-ticket"></i>
            <div class="content" translate="filters.event"></div>
        </h3>
        <facet type="producedIn.exact" data="results"></facet>
    </div>
</div>