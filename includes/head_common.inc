<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="shortcut icon" href="http://marcus.uib.no/img/favicon.ico" type="image/x-icon">
<link rel="icon" href="{% if lodspk.isAdmin %}http://marcus.uib.no/img/favicon-adm.ico"{% else %}http://marcus.uib.no/img/favicon.ico" {% endif %} type="image/x-icon">

<!--<script
  src="https://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>-->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>

<link rel="stylesheet" type="text/css" href="{{lodspk.home}}assets/semantic.min.css">
<link rel="stylesheet" type="text/css" href="{{lodspk.home}}assets/marcus_overrides.css">

<script src="{{lodspk.home}}js/holder.js"></script>

<!-- Font Awesome -->
<link href="//netdna.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">

<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- TODO: Move to pages using it -->
<script type="text/javascript" src="{{lodspk.home}}js/qrcode/jquery.qrcode-0.7.0.min.js"></script>

<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:700|Roboto:400,400i,700,700i|Roboto+Condensed:700,700i&amp;subset=latin-ext" rel="stylesheet"> 

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
