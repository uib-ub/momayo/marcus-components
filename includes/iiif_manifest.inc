<div class="digital-resource" style="height: 50vh;"> <!-- gjelder alle -->
  <div id="uv" class="uv"></div>
</div>

<script src="{{lodspk.home}}scripts/universalviewer/uv/lib/offline.js"></script>
<script src="{{lodspk.home}}scripts/universalviewer/uv/helpers.js"></script>
<link rel="stylesheet" type="text/css" href="{{lodspk.home}}scripts/universalviewer/uv/uv.css">
<script>
  window.addEventListener('uvLoaded', function (e) {
    createUV('#uv', {
      iiifResourceUri: '{{first.manifest.uri.value}}',
      root: '{{lodspk.home}}scripts/universalviewer/uv',
      configUri: '{{lodspk.home}}static/uv-config.json'
    }, new UV.URLDataProvider());
  }, false);
</script>

<script src="{{lodspk.home}}scripts/universalviewer/uv/uv.js"></script>
