// Hjelp, menu item in header-common
$('.help-popup')
  .popup({
    inline   : true,
    hoverable: true,
    position : 'bottom right',
    inverted: true,
    offset: -15,
    delay: {
      show: 300,
      hide: 800
    }
  })
;

// Menu item in header-common
$('.menu-popup')
  .popup({
    inline   : true,
    hoverable: true,
    position : 'bottom left',
    inverted: false,
    variation:'large',
    offset: 0,
    context: '.pusher',
    delay: {
      show: 300,
      hide: 800
    }
  })
;

// Sidebar Responsive
$('.ui.sidebar.responsive')
  .sidebar({
    dimPage: false
  })
  .sidebar('attach events', '#show-menu')
;


// Tab for digitalresources
$('.dr-tab .item')
  .tab()
;

// Download dropdown
$('.download-dropdown')
  .dropdown()
;

// Show/hide for all-data
$('.ui.accordion')
  .accordion()
;

// Download dropdown
$('.dropdown')
  .dropdown({
    keepOnScreen: true
  })
;

// Tabs
$('.menu .item')
  .tab()
;

// Embed videos
$('.ui.embed')
  .embed({
    autoplay: false
  })
;

$('.shape')
  .shape('flip over', {
    width: 'initial'
  })
;